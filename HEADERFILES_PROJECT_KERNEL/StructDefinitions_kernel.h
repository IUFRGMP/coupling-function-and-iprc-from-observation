// ### STRUCTS FOR THE KERNEL METHOD                  ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

#include "baseInclude.h"

//### Dtype for precission controlle ###//
typedef long double DOUB ;

const DOUB pi(atan(1.0)*4.0); //pi

//### DEFINITION OF MAIN DATA STRUCTURE ###
typedef struct VectordataKer
{
  //### Basic data ###//
  vector<DOUB> timeGridHilbert;//raw time 
  vector<DOUB> timeGrid;       //interpolated time ->to kernel
  
  vector<DOUB> timeGridData;   //raw time of Data 
  vector<DOUB> timeGrid2;      //interpolated time of Data -> to kernel
  
  vector<DOUB> phi2;           //raw Hilbert phase
  vector<DOUB> phi;            //interpolated Hilbert phase ->to kernel
  
  vector<DOUB> psi2;           //raw external force phase
  vector<DOUB> psi;            //interpolated external force ->to kernel
  
  vector<DOUB> truephase2;     //only for arctan phase to plot difference 
  vector<DOUB> truephase;      //of obtained phase and true phase from ITHT
    
  vector<DOUB> derive;
  vector<DOUB> derivenew;
  vector<DOUB> Gridphi;
  vector<DOUB> Gridpsi;
  
  //### MAXPOINTS from ITHT ###//
  vector<DOUB> maxpoints;
  
  //### PRC ITERATION ###//
  
  vector<DOUB> PRC;
  vector<DOUB> Force;
  
}VectordataKer;
