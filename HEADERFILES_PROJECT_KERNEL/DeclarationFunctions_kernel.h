// ### FUNCTION DECLARATION FOR KERNEL METHOD         ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

#include "baseInclude.h"

//### DECLARATIONS IN CHRONOLOGICAL ORDER OF THE CODE ###//

//void extractorKer(VectordataKer &Data, string &datafile1);
void extractorKer(VectordataKer &Data, string &datafile1, string &datafile2, string &datafile3, string whichphase1, string dataorHilbert, string whichphase2);
void extractorKer_Len(VectordataKer &Data, string &datafile1, string &datafile2, string &datafile3, string whichphase1, string dataorHilbert, string whichphase2);


DOUB L2(DOUB &y1, DOUB &y2, DOUB &y3, DOUB &t1, DOUB &t2, DOUB &t3, DOUB &t);
void StructresizeKer(VectordataKer &Data, int &M);

void linearFit(DOUB &m, DOUB &n, vector<DOUB> &data, vector<DOUB> &timeGrid, int &start, int &end, DOUB &stepadjust);
DOUB Kernel(DOUB &x, DOUB &y, DOUB &kappa);
DOUB DeriveNew(vector<DOUB> &derive, vector<DOUB> &phi, vector<DOUB> &psi, DOUB &s1, DOUB &s2, DOUB &kappa);

void PRCiterator(VectordataKer &KerData, int &its);
