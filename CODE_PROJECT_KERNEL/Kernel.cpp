// ### DEFINITION OF KERNEL FUNCTION                  ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //
#include "../HEADERFILES_PROJECT_KERNEL/Main_kernel.h"

DOUB Kernel(DOUB &x, DOUB &y, DOUB &kappa)
 {
  return exp(kappa*(cos(x)-1.0))*exp(kappa*(cos(y)-1.0));
 }

DOUB DeriveNew(vector<DOUB> &derive, vector<DOUB> &phi, vector<DOUB> &psi, DOUB &s1, DOUB &s2, DOUB &kappa)
 {
  DOUB sum(0.0);
  DOUB norm(0.0), Del1(0.0), Del2(0.0);
  int cutoff(phi.size()*0.05);//neglect only outer 5 percent for reconstruction
  for(int k=cutoff;k<phi.size()-cutoff; k++)
   {
    Del1=s1-phi[k];
    Del2=s2-psi[k];
    sum += derive[k]*Kernel(Del1,Del2,kappa);
    
    norm += Kernel(Del1,Del2,kappa);
    //cout << "sum= " << sum << " norm= " << norm << "\n"; 
   }
  return sum/norm;
 }

