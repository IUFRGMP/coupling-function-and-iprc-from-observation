// ### DEFINITION OF EXTRACTOR FUNCTION FOR KERNEL    ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

#include "../HEADERFILES_PROJECT_KERNEL/Main_kernel.h"
void extractorKer(VectordataKer &Data, string &datafile1, string &datafile2, string &datafile3, string whichphase1, string dataorHilbert, string whichphase2)
 {
  cout << "reading file 1" << "\n";
  string line1, line2, line3;
  ifstream myfile1 (datafile1);
  ifstream myfile2 (datafile2);
  ifstream myfile3 (datafile3);
    
  //### EXTRACT FILES CONTENT OF FORM TIME <SPACE> DATA \n to struct members ###//
  
  //extract Hilbert Transform Results from ITHT_TO_KERNEL_DATA.<number>.dat
  if(myfile1.is_open())
   {
    double a(0.0), b(0.0), c(0.0), d(0.0), e(0.0), ee(0.0);
    //     time    signal  ITHT    theta   psi
    while(getline(myfile1,line1))
     {
      //cout << line1 << "\n";
      std::stringstream ss1(line1);
      ss1 >> a >> b >> c >> d >> e; 
      Data.timeGrid.push_back(a);
      if(whichphase1=="cleansed"){Data.phi.push_back(e);}//take cleasned phase NOTE: CHANGE BACK TO e!!!
      else{Data.phi.push_back(d);}                       //take uncleansed phase
     }
    myfile1.close();
   } 
    
  cout << "reading file 2 \n";
  //extract Phase of Forcing from dataGen<number>.dat
  if(myfile2.is_open())
   {
    double f(0.0), g(0.0), h(0.0), p(0.0), s(0.0);
    //     sim time, signal, phase of force, true phase of system -> if Here a file with raw data is given 
    //     time    signal  ITHT    theta   psi                    -> if second Hilbert file is given for pairwise interaction
    
    int ml=0;
    while(getline(myfile2,line2))
     {
      cout << line2 << "\n";
      std::stringstream ss2(line2);
      ss2 >> f >> g >> h >> p >> s;
      Data.timeGridData.push_back(f);//time line of data generation
      //Data.psi2.push_back(h);//phase of external force
      //Data.truephase2.push_back(h);
     if(dataorHilbert=="Hilbert")                                    //take second Hilbert file
       {
        if(whichphase2=="cleansed"){Data.truephase2.push_back(s);}    //then take cleansed phase
        else{Data.truephase2.push_back(p);}                           //or take uncleansed phase
       }
      else                                                            //or take second file to be a data file
       {
        if(whichphase2=="asympthotic"){Data.truephase2.push_back(p);} //use the asymphtotic phase
        else{Data.truephase2.push_back(h); cout << "ext force at\n";}                           //use the phase of the external force
       }
    cout << " file 2 at line " << ml << "\n";
    ml++;
     }
    myfile2.close();
   } 
   
   //### NOW PICK OUT ONLY THOSE POINTS THAT ARE ON UNIFORMELY SPACED TIME GRID ###//
   
   //NOTE: Step is necessary since if second Hilbert file is given and not data, the secondHilbert file will have 
           // separate interpolation times and separte interpolation values if you compare it to the first Hilbert 
           // file. Therefore, here first all interpolation times and interpolation values are removed Assumptions:
           // time intervals in both files are the same, the first interpoaltion time is not in the first two points
   
   cout << "sifting of file 2 time grid \n";
   double meandt(0.0); meandt +=Data.timeGrid[1]; meandt -=Data.timeGrid[0];
   
   int l(0),ll(0);
   while(l<Data.truephase2.size())
   {  
    if(abs(Data.timeGridData[ll]-meandt*double(l)-Data.timeGridData[0])<0.00001)//time on equal spacing
     {
      Data.psi2.push_back(Data.truephase2[ll]);
      Data.timeGrid2.push_back(Data.timeGridData[ll]);
      ll +=1;
      l+=1;
     }
    else
     {
      ll+=1;
     }
   }
   
  cout << "reading file 3\n"; 
  //extract interpolation times from <number>maxinterpolation.dat
  if(myfile3.is_open())
   {
    double q(0.0), r(0.0);
    //     interpolation time, interpolation value,interpolation index
    while(getline(myfile3,line3))
     {
      std::stringstream ss3(line3);
      ss3 >> q >> r ;
      Data.maxpoints.push_back(q);//time line of data generation
     }
    myfile3.close();
   }   
    
  //shift initial time to Hilber time
  DOUB timeofset(Data.timeGrid2[0]);
  DOUB timeofsetHilbert(Data.timeGrid[0]);
  cout << "t0HILBERT= " << timeofsetHilbert << " time ofset data= " << timeofset << "\n";
  
  for(int k=0; k<Data.timeGrid2.size();k++)
   {
    Data.timeGrid2[k] -= timeofset;
   }
    
  //determine start time of Hilbert time series in initial Time series
  int start(0);
  for(int k=0;k<Data.timeGrid2.size();k++)
   {
    if(abs(Data.timeGrid2[k]-timeofsetHilbert)<0.000001)//ignor rounding errors
     {
      start=k;
      break;
     }
   }
   cout << "start= " << start << "\n";
   
  //shift maxtimes with respect to hilbert cutof time at beginning find first relevant max point index.
  int counter(0);
  for(int k=0; k<Data.maxpoints.size();k++)
   {
    if((Data.maxpoints[k] >=Data.timeGrid[0]))//find first maxtime which is in the hilbert time range
     {
      counter=k;
      cout << "FOUND counter = " << counter << " Data.timeGrid[0]= " << Data.timeGrid[0] << " Data.maxpoints[counter]= " << Data.maxpoints[counter] << " length of vectors has to be th esame: " << Data.timeGrid.size() << " " << Data.phi.size() << " " << Data.timeGridData.size() << " " << Data.truephase2.size() << " " << Data.psi2.size() << " " << Data.timeGrid2.size() << " " << Data.maxpoints.size() << "\n";
      break;
     }
   }
  
  //fill real Data vectors starting from Hilbert start time, Interpolate psi at max points
  
  for(int k=start; k<Data.psi2.size()-start;k++)
   {
    Data.psi.push_back(Data.psi2[k]);            //interpolate external force phase at extra points
    Data.truephase.push_back(Data.truephase2[k]);//interpolate true phase at extra points

    if((Data.timeGrid2[k]<Data.maxpoints[counter]) & (Data.timeGrid2[k+1]>Data.maxpoints[counter]))// & (Data.timeGrid[k+1]<Data.timeGrid2[k+1])) //interpolate maximum time point also for psi
     {
      cout << "INTERPOLATE PHASE HERE ################################ " << Data.maxpoints[counter]  << " length of vectors has to be th esame: " << Data.timeGrid.size() << " " << Data.phi.size() << " " << Data.timeGridData.size() << " " << Data.truephase2.size() << " " << Data.psi2.size() << " " << Data.timeGrid2.size() << " " << Data.maxpoints.size() << "\n";
      Data.psi.push_back(L2(Data.psi2[k], Data.psi2[k+1], Data.psi2[k+2], Data.timeGrid2[k], Data.timeGrid2[k+1], Data.timeGrid2[k+2], Data.maxpoints[counter]));
      Data.truephase.push_back(L2(Data.truephase2[k], Data.truephase2[k+1], Data.truephase2[k+2], Data.timeGrid2[k], Data.timeGrid2[k+1], Data.timeGrid2[k+2], Data.maxpoints[counter]));
      counter += 1;
     }
   }
  
  cout << "INTERPOLATION FINISHED ######################### \n";
  cout << "SUBTRACT START VALUE RESET PHASES TO ZERO AT t=0  ######################### \n";  
  DOUB mm(0.0), nn(0.0), stepadjust(0.0);
  
  for(int k=0;k<Data.phi.size();k++)
   {
    Data.timeGrid[k] -= timeofsetHilbert;
   }
 cout << "EXTRACTOR FINISHED \n";

 }
 
void extractorKer_Len(VectordataKer &Data, string &datafile1, string &datafile2, string &datafile3, string whichphase1, string dataorHilbert, string whichphase2)
 {
  cout << "reading a file" << "\n";
  string line1, line2, line3;
  ifstream myfile1 (datafile1);
  ifstream myfile2 (datafile2);
  ifstream myfile3 (datafile3);
    
  //### EXTRACT FILES CONTENT OF FORM TIME <SPACE> DATA \n to struct members ###//
  
  std::stringstream out0; out0 << "extractorHilbert.dat";
  std::string name0 = out0.str(); ofstream debugg(name0,ios::out);
  debugg.setf(ios::scientific); debugg.precision(15);
  
  //extract Hilbert Transform Results from ITHT_TO_KERNEL_DATA.<number>.dat
  if(myfile1.is_open())
   {
    double a(0.0), b(0.0), c(0.0), d(0.0), e(0.0), ee(0.0);
    //     time    signal  ITHT    theta   psi     true phase
    while(getline(myfile1,line1))
     {
      //cout << line1 << "\n";
      std::stringstream ss1(line1);
      ss1 >> a >> b >> c >> d >> e >> ee; 
      Data.timeGridHilbert.push_back(a);
      if(whichphase1=="cleansed"){Data.phi.push_back(e);}
      else{Data.phi.push_back(d);}
      //Data.phi2.push_back(e);//###################comment out for asymptotic phase from data in kernel reconstruction
      debugg << a << " " << e << "\n"; 
     }
    myfile1.close();
   } 
  debugg.close();
   
  std::stringstream out1; out1 << "extractorData.dat";
  std::string name1 = out1.str(); ofstream debugg2(name1,ios::out);
  debugg2.setf(ios::scientific); debugg2.precision(15);

  //extract Phase of Forcing from dataGen<number>.dat
  if(myfile2.is_open())
   {
    double f(0.0), g(0.0), h(0.0), p(0.0), s(0.0);
    //     sim time, signal, phase of force, true phase of system
    while(getline(myfile2,line2))
     {
      //cout << line2 << "\n";
      std::stringstream ss2(line2);
      ss2 >> f >> g >> h >> p >> s;
      Data.timeGridData.push_back(f);//time line of data generation
      Data.psi2.push_back(h);//phase of external force without extra points
      Data.truephase2.push_back(p);
      //Data.phi2.push_back(p);//############################comment in for asymptotic phase in kernel reconstruction
      if(dataorHilbert=="Hilbert")
       {
        if(whichphase2=="cleansed"){Data.psi2.push_back(s);}
        else{Data.psi2.push_back(p);}
       }
      else
       {
        if(whichphase2=="asympthotic"){Data.psi2.push_back(p);}
        else{Data.psi2.push_back(h);}
       }
      debugg2 << f << " " << h << "\n";
     }
    myfile2.close();
   } 
  debugg2.close();
    
  //### Interpolate time in psi grid ###//
  //Task: find the period time, where the phase reachse exctly 2pi
  //Write this time to the Hilbert times 
  //Write all n2pi to the obtained Hilbert phase
  //Interpolate also the external force at n2pi of the phase

  std::stringstream out2; out2 << "extraPoints.dat";
  std::string name2 = out2.str(); ofstream debugg3(name2,ios::out);
  debugg3.setf(ios::scientific); debugg3.precision(15);
  
  DOUB Atphase(2.0*pi);  
  for(int k=0; k<Data.timeGridHilbert.size()-1;k++)
   {
    //perform copy
    Data.phi.push_back(Data.phi2[k]);
    Data.timeGrid.push_back(Data.timeGridHilbert[k]);
    Data.psi.push_back(Data.psi2[k]);
    Data.timeGrid2.push_back(Data.timeGridData[k]);
    
    //if((Data.phi2[k] > 1.5*Atphase)&(Data.phi2[k] < 2.5*Atphase)) cout << "I copy Sier! (Data.phi2[k] " << Data.phi2[k] << " < Atphase" << 2.0*Atphase << ") & (Data.phi2[k+1] " << Data.phi2[k+1] << " > Atphase " << 2.0*Atphase<< " )" << "\n";
      
    if((Data.phi2[k+1] > Atphase))
     {
      cout << "interpolate at k= << " << k <<": Data.phi2[k+1]= " << Data.phi2[k+1] << ">" << Atphase << "\n";
      //add n2pi to phase; interpolate time of this phase
      Data.phi.push_back(Atphase);//Hilbert phase 
      Data.timeGrid.push_back(L2(Data.timeGridHilbert[k], Data.timeGridHilbert[k+1], Data.timeGridHilbert[k+2], Data.phi2[k], Data.phi2[k+1], Data.phi2[k+2], Atphase));//time Grid of ITHT
      Data.timeGrid2.push_back(L2(Data.timeGridData[k], Data.timeGridData[k+1], Data.timeGridData[k+2], Data.phi2[k], Data.phi2[k+1], Data.phi2[k+2], Atphase));//time Grid of data
      Data.psi.push_back(L2(Data.psi2[k], Data.psi2[k+1], Data.psi2[k+2], Data.phi2[k], Data.phi2[k+1], Data.phi2[k+2], Atphase));//external Force
      
      debugg3 << L2(Data.timeGridHilbert[k], Data.timeGridHilbert[k+1], Data.timeGridHilbert[k+2], Data.phi2[k], Data.phi2[k+1], Data.phi2[k+2], Atphase) << " " << Atphase << "\n";
      Atphase +=2.0*pi;
     }
   }
  Data.phi.push_back(Data.phi2[Data.timeGridHilbert.size()-1]);
  Data.timeGrid.push_back(Data.timeGridHilbert[Data.timeGridHilbert.size()-1]);
  Data.psi.push_back(Data.psi2[Data.timeGridHilbert.size()-1]);
  Data.timeGrid2.push_back(Data.timeGridData[Data.timeGridHilbert.size()-1]);
  
  debugg3.close();
  
  DOUB timeofset(Data.timeGrid2[0]);
  for(int  k=0;k<Data.timeGrid2.size();k++)
   {
    Data.timeGrid2[k] -=timeofset;
   }
  cout << "reading finished \n";
}
 
 void StructresizeKer(VectordataKer &Data, int &M)
  {
   Data.psi.resize(Data.phi.size());
   Data.Gridphi.resize(M);
   Data.Gridpsi.resize(M);
   Data.derive.resize(Data.phi.size());
   Data.derivenew.resize(Data.Gridphi.size()*Data.Gridphi.size()); //vectorized matrix of phase derivative
   Data.PRC.resize(M);
   Data.Force.resize(M);
  }
  
DOUB L2(DOUB &y1, DOUB &y2, DOUB &y3, DOUB &t1, DOUB &t2, DOUB &t3, DOUB &t)
 {
   return (t-t2)*(t-t3)/((t1-t2)*(t1-t3))*y1 + (t-t1)*(t-t3)/((t2-t1)*(t2-t3))*y2 + (t-t1)*(t-t2)/((t3-t1)*(t3-t2))*y3;
 }
