// ### DEFINITION OF MAIN FUNCTION FOR KERNEL METHOD  ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

 #include "../HEADERFILES_PROJECT_KERNEL/Main_kernel.h"
 
 int main(int argc, char* argv[])
 {
  cout << "START OF KERNEL DECOMPOSITION" << "\n";
  
  //### INITIALIZE DATA STRUC ###//
  
  VectordataKer KerData;
    
  //### EXTRACT DATA FILE ###//
  DOUB kappa(0.0);
  string datafile1     = argv[1];  //Hilberttransform result (final)  ITHT_TO_KERNEL_DATA<number>.dat
  string datafile2     = argv[2];  //DataGeneration result dataGen<number>.dat
  string datafile3     = argv[3];  //maximum points from End of Hilbert transform <number>maxinterpolation.dat
  string whichphase1   = argv[8];  //input is "cleansed" or other string ->then uncleansed phase is used
  string dataorHilbert = argv[9];  //input is "Hilbert" or other string ->then data file is used            NOTE: for the moment: cleansed, other string, other string only due to interpolation issues in the extraction process
  string whichphase2   = argv[10]; //input is "cleansed" or "asympthotic" >then (only if data file is used) asymptotic phase is used or other string, then external force is used
   
  kappa = strtod(argv[4],NULL);    //give smoothing value
  //NOTE: give also argv[5] as the number of the experiment
  int its = int(strtod(argv[6],NULL)); //number of iterates
  int M=int(strtod(argv[7],NULL)); //discretization of 2pi
  
  DOUB eps = strtod(argv[11],NULL); //giving the reference normalization value of forcing -> amplitude or amplitude*integral

  //extractorKer_Len(KerData, datafile1, datafile2, datafile3,whichphase1, dataorHilbert, whichphase2);//comment in for length phase or/and true phase
  //extractorKer(KerData, datafile1, datafile2, datafile3);//comment in for arctan phase
  extractorKer(KerData, datafile1, datafile2, datafile3, whichphase1, dataorHilbert, whichphase2);
  
  std::stringstream out0; out0 << "debugg_kappa" << argv[4] << "." << argv[5] << ".dat";
  std::string name0 = out0.str(); ofstream debugg(name0,ios::out);
  debugg.setf(ios::scientific); debugg.precision(15);
  
   for(int k=0; k<KerData.phi.size(); k++)
   {
    debugg << KerData.timeGrid[k] << " " << KerData.timeGrid2[k] << " " <<  KerData.phi[k] << " " << KerData.psi[k] << "\n";
    //        time  interpolated            time not interpolated           hilbert phase            external force phase
   } 
   
  //### PREPARE PHASE GRID ###//
  
  cout << "prepare phase grid \n";
  
  
  StructresizeKer(KerData, M);

  for(int k=0; k<M; k++)
   {
    KerData.Gridphi[k] = 2.0*pi*DOUB(k)/(DOUB(M));
    KerData.Gridpsi[k] = 2.0*pi*DOUB(k)/(DOUB(M));
   }
   
  //### CALCULATE DERIVATIVE ###//
  
  cout << "calculate drivative \n";
    
  KerData.derive[0] = (KerData.phi[1]-KerData.phi[0])/(KerData.timeGrid[1]-KerData.timeGrid[0]);
  
  for(int k=1; k<KerData.phi.size()-1;k++)
   {
    KerData.derive[k] = (KerData.phi[k+1]-KerData.phi[k-1])/(KerData.timeGrid[k+1]-KerData.timeGrid[k-1]);
   }
   
  KerData.derive[KerData.derive.size()-1] = (KerData.phi[KerData.derive.size()-1]-KerData.phi[KerData.derive.size()-2])/(KerData.timeGrid[KerData.derive.size()-1]-KerData.timeGrid[KerData.derive.size()-2]);
  
  //### PERFORM KERNEL SMOOTHING OF DERIVATIVE ###//
  
  cout << "kernel smoothing \n";
  
  DOUB s1(0.0), s2(0.0);
  int counter(0);
  
  std::stringstream out1; out1 << "Couplingfunction"<< argv[5] <<"_kappa" << argv[4] << ".dat";
  std::string name1 = out1.str(); ofstream hypsurf(name1,ios::out);
  hypsurf.setf(ios::scientific); hypsurf.precision(15);
  
  for(int k=0; k<KerData.Gridphi.size(); k++) 
   {
    s1=KerData.Gridphi[k];
    //cout << "s1= " << s1 << "\n";
    for(int m=0; m<KerData.Gridphi.size(); m++)
     {
      s2=KerData.Gridpsi[m];
      KerData.derivenew[counter] = DeriveNew(KerData.derive, KerData.phi, KerData.psi, s1, s2, kappa);         //matrix is vectorized here with index counter
      hypsurf << KerData.Gridphi[k] << " " << KerData.Gridpsi[m] << " " << KerData.derivenew[counter] << "\n";
      counter+=1;
     }
    hypsurf << "\n";
   }
   
  std::stringstream out2; out2 << "COUPLING"<< argv[5] << "_kappa" << argv[4] << ".dat";
  std::string name2 = out2.str(); ofstream PRCf(name2,ios::out);
  PRCf.setf(ios::scientific); PRCf.precision(15);
  
  std::stringstream out3; out3 << "PRC"<< argv[5] << "_kappa" << argv[4] << ".dat";
  std::string name3 = out3.str(); ofstream PRC2f(name3,ios::out);
  PRC2f.setf(ios::scientific); PRC2f.precision(15);
  
  std::stringstream out4; out4 << "FORCE"<< argv[5] << "_kappa" << argv[4] << ".dat";
  std::string name4 = out4.str(); ofstream PRC3f(name4,ios::out);
  PRC3f.setf(ios::scientific); PRC3f.precision(15);
      
  PRCiterator(KerData,its);// perform iterative decomposition
  
  cout << "output of data \n";
  
  for(int k=0; k<KerData.Gridphi.size();k++)//print to file the coupling function (PRC) in Winfree form
   { 
    for(int m=0; m<KerData.Gridpsi.size();m++)
     {
      PRCf << KerData.Gridphi[k] << " " << KerData.Gridpsi[m] << " " << KerData.PRC[k]*KerData.Force[m] << "\n";
     }
    PRCf << "\n";
   }
 
  //### REMOVE MULTIPLICATIVE AMBIGUITY ###//
  
  DOUB hat_P(1.0); //norm of reconstructed force
 // for(int k=0; k<KerData.Gridpsi.size()-1;k++)
 //  {
 //   hat_P += 0.5*(KerData.Force[k+1]*KerData.Force[k+1]+KerData.Force[k]*KerData.Force[k])*(KerData.Gridpsi[k+1]-KerData.Gridpsi[k]);
 //  }
 // hat_P=sqrt(hat_P);

  //### output of results ###//

  for(int k=0; k<KerData.Gridphi.size();k++)//print iPRC Z(phi)
   { 
    PRC2f << KerData.Gridphi[k] << " " << KerData.PRC[k]*hat_P/eps << "\n";
   } 
   
  for(int m=0; m<KerData.Gridpsi.size();m++)//print external force
   {
    PRC3f << KerData.Gridpsi[m] << " " << KerData.Force[m]*eps/hat_P << "\n";  
   }
  
  PRCf.close();
  PRC2f.close();
  PRC3f.close();
  hypsurf.close();
  debugg.close();
  
  return 0;
 }
