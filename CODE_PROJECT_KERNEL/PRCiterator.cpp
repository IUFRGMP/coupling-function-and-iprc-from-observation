// ### DEFINITION OF PRC ITERATOR                     ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //
#include "../HEADERFILES_PROJECT_KERNEL/Main_kernel.h"

void linearFit(DOUB &m, DOUB &n, vector<DOUB> &data, vector<DOUB> &timeGrid, int &start, int &end, DOUB &stepadjust)
 {
  DOUB S(0.0), Sx(0.0), Sy(0.0), Sxx(0.0), Sxy(0.0);
  for(int k=start; k<=end; k++)
   {
    S   += 1.0/(DOUB(end-start));
    Sx  += timeGrid[k]/(DOUB(end-start));
    Sy  += data[k]/(DOUB(end-start));
    Sxx += timeGrid[k]*timeGrid[k]/(DOUB(end-start));
    Sxy += timeGrid[k]*data[k]/(DOUB(end-start));
   }
  m = (S*Sxy - Sx*Sy)/(S*Sxx - Sx*Sx);// slope
  n = (Sxx*Sy - Sx*Sxy)/(S*Sxx - Sx*Sx);// abs val
 }

void PRCiterator(VectordataKer &KerData, int &its)
 {
  int offset(1000);
  
  DOUB OMEG(0.0);//=(KerData.phi[KerData.phi.size()-offset]-KerData.phi[offset])/(KerData.timeGrid[KerData.timeGrid.size()-offset]-KerData.timeGrid[offset]);
  DOUB stepadjust(0.0), phi0(0.0);;
  int start(0.1*KerData.phi.size()), end(0.9*KerData.phi.size());//fit mean phase growth with linear lest squares fit
  linearFit(OMEG, phi0, KerData.phi, KerData.timeGrid, start, end, stepadjust);

  cout << "OMEG= " << OMEG << "\n";
  
  //set force to 1 uniformely or something else
  for(int k=0; k< KerData.Force.size(); k++)
   {
    KerData.Force[k] = (1.0 + 0.01*sin(DOUB(k))); //(0.1 + DOUB(k)/(DOUB(KerData.Force.size())));//for Force=sin(t) this is not ortogonal
   }
  
  DOUB Norm(0.0);
  DOUB Int(0.0);
  DOUB Delpsi(KerData.Gridpsi[1]-KerData.Gridpsi[0]);
  DOUB Delphi(KerData.Gridphi[1]-KerData.Gridphi[0]);
  int indexphi(0);
  
  //calculate ||Force||^2
  Norm=0.0;
  for(int k=0; k<KerData.Gridpsi.size()-1; k++)//integrate over psi at fixed phi
   {
    Norm += (KerData.Force[k]*KerData.Force[k]+KerData.Force[k+1]*KerData.Force[k+1])*Delpsi*0.5;
   }
  
  //### ITERATE DECOMPOSITION ###//
  
  for(int n=0; n<its; n++)
   {  
    for(int m=0; m<KerData.Gridphi.size(); m++)//iterate for PRC over all phi
     {
      Int=0.0;
      for(int k=m*KerData.Gridpsi.size(); k<(m+1)*KerData.Gridpsi.size()-1; k++)//integrate over psi at fixed phi
       {
        Int += ((KerData.derivenew[k]-OMEG)*KerData.Force[k-m*KerData.Gridpsi.size()]+(KerData.derivenew[k+1]-OMEG)*KerData.Force[k-m*KerData.Gridpsi.size()+1])*Delpsi*0.5;
        //cout << "KerData.derivenew[" << k << "]= " << KerData.derivenew[k] << " Int= " << Int << " Norm= " << Norm << "\n";
       }
      KerData.PRC[m] = Int/(Norm);//PRC iterate is given by this
     } 
   
    cout << "################## \n";
    
    Norm=0.0;
    for(int m=0; m<KerData.Gridphi.size()-1; m++)//integrate over psi at fixed phi
     {
  //  Norm += (KerData.Force[m]*KerData.Force[m]+KerData.Force[m+1]*KerData.Force[m+1])*Delpsi*0.5;
      Norm += (KerData.PRC[m]*KerData.PRC[m]+KerData.PRC[m+1]*KerData.PRC[m+1])*Delpsi*0.5;
     }
    
    for(int k=0; k<KerData.Gridpsi.size();k++)//iterate for Force over all psi
     {
      Int=0.0;
      indexphi=0;
      for(int m=k ; m<k+KerData.derivenew.size()-KerData.Gridpsi.size(); m=m+KerData.Gridpsi.size()) //integrate over all  phi at fixed psi
       {
        Int += ((KerData.derivenew[m]-OMEG)*KerData.PRC[indexphi]+(KerData.derivenew[m+KerData.Gridpsi.size()]-OMEG)*KerData.PRC[indexphi+1])*Delphi*0.5;
        //acout << "KerData.derivenew[" << m << "]= " << KerData.derivenew[m] << " Int= " << Int << " Norm= " << Norm << "\n";
        indexphi +=1;
       }
      KerData.Force[k] = Int/(Norm);
     }

    Norm=0.0;
    for(int k=0; k<KerData.Gridpsi.size()-1; k++)//integrate over psi at fixed phi
     {
  //  Norm += (KerData.Force[k]*KerData.Force[k]+KerData.Force[k+1]*KerData.Force[k+1])*Delpsi*0.5;
      Norm += (KerData.Force[k]+KerData.Force[k+1])*Delpsi*0.5;
     }
   }
 }



