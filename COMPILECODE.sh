 
 #this script compiles the code for Kernel reconstruction of the pairwise coupling function
  
 HEADERPATH='../HEADERFILES_PROJECT_KERNEL/'
 CODEPATH='../CODE_PROJECT_KERNEL/'
 
 g++ $HEADERPATH/Main_kernel.h $CODEPATH/Kernel.cpp $CODEPATH/ExtractorKer.cpp $CODEPATH/PRCiterator.cpp $CODEPATH/Main_kernel.cpp -o ker -std=c++11 -O
